//
//  TVShow.swift
//  TVShower
//
//  Created by Ninja on 6/13/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

struct TVShow: Codable {
    let id: Int
    let url,name: String
    let rating: Rating
    let image: Image
    let summary: String
    let premiered: String
    
    struct Rating: Codable {
        let average: Double
    }
    
    struct Image: Codable {
        let medium: String
        let original: String
    }
}
