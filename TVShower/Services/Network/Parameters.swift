//
//  Parameters.swift
//  TVShower
//
//  Created by Ninja on 6/13/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation


struct Parameters {
    var type: String
    var body:[String: Any]
}
