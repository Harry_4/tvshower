//
//  NetworkRequest.swift
//  TVShower
//
//  Created by Ninja on 6/13/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

protocol HTTPRequest {
    func makeURLRequest(parameters: Parameters?) -> URLRequest?
}

class NetworkRequest: HTTPRequest {
    private var resourcePath: String
    init(urlPath: String) {
        resourcePath = urlPath
    }
    
    var httpRequest: URLRequest? {
        guard let reqUrl = URL(string: resourcePath) else {
            return nil
        }
        return URLRequest(url: reqUrl)
    }
    
    func makeURLRequest(parameters: Parameters?) -> URLRequest? {
        var urlComponents = URLComponents(string: resourcePath)
        var paramIterator = parameters?.body.makeIterator()
        while let nextItem = paramIterator?.next() {
            urlComponents?.queryItems = [
                URLQueryItem(name: nextItem.key, value: nextItem.value as? String)
            ]
        }
        guard let resURL = urlComponents?.url else { return nil }
        
        return URLRequest(url: resURL)
        
    }
}
