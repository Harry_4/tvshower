//
//  HTTPClient.swift
//  TVShower
//
//  Created by Ninja on 6/13/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

protocol HTTPClient {
    func request(request: HTTPRequest, type: HTTPMethod, params: Parameters?, completionHandler: @escaping NetworkResponseHandler)
    func cancelCurrentRequest()
}

enum HTTPMethod: String {
    case get
    case post
}

typealias NetworkResponseHandler = (Data?,URLResponse?,Error?) -> Void

class NetworkService: HTTPClient{
    private let urlSession: URLSession
    private var currentTask: URLSessionTask?
    
    init() {
        self.urlSession = URLSession.shared
    }

    func request(request: HTTPRequest, type: HTTPMethod, params: Parameters? = nil, completionHandler: @escaping NetworkResponseHandler) {
        guard let urlRequest = request.makeURLRequest(parameters: params) else {
            return
        }
        currentTask = urlSession.dataTask(with: urlRequest) { (responseData, urlResponse, responseError) in
            completionHandler(responseData, urlResponse, responseError)
        }
        currentTask?.resume()
    }
    
    func cancelCurrentRequest() {
        currentTask?.cancel()
    }
    
}
