//
//  DateHelper.swift
//  TVShower
//
//  Created by Ninja on 6/15/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

extension Date {
    static func daysSince(fromDate: Date, toDate: Date, adjustBy days: Int) -> Int {
        return Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day ?? 0
    }
}

func daysElapsed(from date: Date) -> Int {
    return Date.daysSince(fromDate: date, toDate: Date(), adjustBy: 0)
}
