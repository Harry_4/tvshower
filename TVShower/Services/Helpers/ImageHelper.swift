//
//  ImageHelper.swift
//  TVShower
//
//  Created by Ninja on 6/16/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation
import UIKit

class ImageHelper {
    
    private var cache = NSCache<AnyObject, AnyObject>()
    
    init() {
        cache.countLimit = 10
    }
    
    func download(with url: URL, completionHandler:@escaping (UIImage?) -> Void) {
        guard let cachedImg = cache.object(forKey: url as AnyObject) else {
            DispatchQueue.global(qos: .userInitiated).async {
                guard let imgData = try? Data(contentsOf: url) else {
                    completionHandler(nil)
                    return
                }
                let img = UIImage(data: imgData)
                if (img != nil) {
                    self.cache.setObject(url as AnyObject, forKey: img!)
                }
                completionHandler(img)
            }
            return
        }
        completionHandler(cachedImg as? UIImage)
    }

}
