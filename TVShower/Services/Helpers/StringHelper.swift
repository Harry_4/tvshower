//
//  StringHelper.swift
//  TVShower
//
//  Created by Ninja on 6/16/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

public func prettyString(from numberValue: Int) -> String? {
    let nf = NumberFormatter()
    nf.numberStyle = .decimal
    let formatedDays = nf.string(from: NSNumber(value: numberValue)) ?? "-"
    return "Days since the show first premiered: " + formatedDays
}
