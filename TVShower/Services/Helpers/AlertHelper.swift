//
//  AlertControllerHelper.swift
//  TVShower
//
//  Created by Ninja on 6/15/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation
import UIKit

public class AlertHelper {
    private var alertController: UIAlertController!
    
    init(alertType: UIAlertController.Style) {
        alertController = UIAlertController(title: nil, message: nil, preferredStyle: alertType)
    }
    
    public func makeAlert(with title: String?, message: String?) -> UIAlertController {
        alertController.title = title
        alertController.message = message
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        return alertController
    }
    
    public func makeActionSheet() -> UIAlertController {
//        TODO:- Add implementation for action sheet
        return alertController
    }
}
