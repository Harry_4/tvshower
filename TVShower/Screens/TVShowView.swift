//
//  TVShowView.swift
//  TVShower
//
//  Created by Ninja on 6/14/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

class TVShowView: UIView {

    @IBOutlet weak var tvShowImage: UIImageView!
    @IBOutlet weak var showTitle: UILabel!
    @IBOutlet weak var showSummary: UILabel!
    @IBOutlet weak var daysSince: UILabel!
    
    private let imghelper = ImageHelper()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func loadView(with model: TVShowViewModel) {
        showTitle.text = model.showName
        daysSince.text = model.daysSince
        showSummary.text = model.showSummary
        
        guard let urlString = model.showUrl, let imgURL = URL(string: urlString) else {
            return
        }
        imghelper.download(with: imgURL) { [weak self] (downloadedImg) in
            guard let strongSelf = self else { return }
            DispatchQueue.main.async {
                UIView.transition(with: strongSelf.tvShowImage,
                                  duration: 1.0,
                                  options: [.curveEaseOut, .transitionCrossDissolve],
                                  animations: {
                                    strongSelf.tvShowImage.image = downloadedImg
                })
            }
        }
    }
}
