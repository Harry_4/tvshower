//
//  ViewController.swift
//  TVShower
//
//  Created by Ninja on 6/13/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    private var httpClient = NetworkService()
    private var tvShowViewModel: TVShowViewModel!
    
    private lazy var errorAlert: UIAlertController = {
        let alertHelper = AlertHelper(alertType: .alert)
        return alertHelper.makeAlert(with: "Error", message: "Show not found!")
    }()
    
    private var searchController: UISearchController = {
        let controller = UISearchController(searchResultsController: nil)
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.autocapitalizationType = UITextAutocapitalizationType.none
        return controller
    }()
    
    private lazy var tvShowView: TVShowView = {
        let view = UINib(nibName: "TVShowView", bundle: Bundle.main).instantiate(withOwner: TVShowView.self, options: nil)[0] as! TVShowView
        return view
    }()
    
    private var emptyView: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = UIColor(red: 150/255.0, green: 220/255.0, blue: 255/255.0, alpha: 1.0)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let label = UILabel(frame: .zero)
        label.text = "Use Search Bar to search for TV Shows"
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        definesPresentationContext = true
        self.navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
        
        tvShowViewModel = TVShowViewModel(client: httpClient)
        
        self.view.addSubview(emptyView)
        emptyView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        emptyView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        emptyView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        emptyView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchRequest = NetworkRequest(urlPath: "http://api.tvmaze.com/singlesearch/shows")
        let parameters = Parameters(type: "query", body: ["q" : searchBar.text ?? ""])
        tvShowViewModel.fetchShow(searchRequest, parameters: parameters, completionHandler: { [weak self] (model, err) in
            guard let strongSelf = self else { return }
            if(err != nil || model == nil) {
                DispatchQueue.main.async {
                    strongSelf.present(strongSelf.errorAlert, animated: true, completion: nil)
                }
            }
            else {
                strongSelf.tvShowViewModel = model!
                DispatchQueue.main.async {
                    strongSelf.add(view: strongSelf.tvShowView)
                    strongSelf.load(strongSelf.tvShowView, with: strongSelf.tvShowViewModel)
                }
            }
        })
    }
    
    
    public func add(view: UIView) {
        if(!(self.view.subviews.first is TVShowView)) {
            self.view.addSubview(tvShowView)
        }
    }
    
    public func load(_ view: UIView, with model: TVShowViewModel) {
        guard let showView = view as? TVShowView else { return }
        UIView.transition(with: tvShowView, duration: 1.0, options: [.curveEaseInOut,.transitionCurlDown], animations: {
            showView.loadView(with: model)
        }, completion: nil)
        
    }

}
