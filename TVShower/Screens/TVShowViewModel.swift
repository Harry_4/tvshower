//
//  TVShowViewModel.swift
//  TVShower
//
//  Created by Ninja on 6/14/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import Foundation

public class TVShowViewModel {
    
    private var tvShow: TVShow?
    private var httpClient: NetworkService
    private let decoder = JSONDecoder()
    private var dateFormat = "yyyy-MM-dd"
    private lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = dateFormat
        return df
    }()
    
    init(show: TVShow? = nil, client: NetworkService) {
        tvShow = show
        httpClient = client
    }
    
    var showName: String? {
        return tvShow?.name
    }
    
    var showSummary: String? {
        return tvShow?.summary.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    var showUrl: String? {
        return tvShow?.image.original
    }
    
    var daysSince: String? {
        guard let show = tvShow else { return nil }
        guard let premierDate = dateFormatter.date(from: show.premiered) else { return nil }
        let days = daysElapsed(from: premierDate)
        return prettyString(from: days)
    }
    
    func fetchShow(_ showReq: NetworkRequest, parameters: Parameters, completionHandler: @escaping (TVShowViewModel?, Error?) ->Void) {
        httpClient.request(request: showReq, type: .get, params: parameters) { [weak self](data, res, err) in
            guard let strongSelf = self else { return }
            guard let strongData = data else {
                completionHandler(nil,err)
                return
            }
            do {
                let tvShow = try strongSelf.decoder.decode(TVShow.self, from: strongData)
                strongSelf.tvShow = tvShow
                completionHandler(self,nil)
            }
            catch {
                completionHandler(nil,error)
            }
            
        }
    }
}
