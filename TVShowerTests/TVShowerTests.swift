//
//  TVShowerTests.swift
//  TVShowerTests
//
//  Created by Ninja on 6/13/19.
//  Copyright © 2019 Ninja. All rights reserved.
//

import XCTest
@testable import TVShower

class TVShowerTests: XCTestCase {

    var vc: ViewController!
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        vc = (UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "TVShowerVC") as! ViewController)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTVShowView() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let view = UINib(nibName: "TVShowView", bundle: Bundle.main).instantiate(withOwner: TVShowView.self, options: nil)[0] as? TVShowView
        XCTAssertNotNil(view)
        vc.add(view: view!)
        XCTAssert(vc.view.subviews.last is TVShowView, "Failed to add tvshow view")
    }
    
    func testTVShowViewModel() {
        let model = TVShow(id: 123, url: "", name: "Test", rating: TVShow.Rating(average: 10.0), image: TVShow.Image(medium: "", original: ""), summary: "test summary", premiered: "2017-06-24")
        let viewModel = TVShowViewModel(show: model, client: NetworkService())
        let view = UINib(nibName: "TVShowView", bundle: Bundle.main).instantiate(withOwner: TVShowView.self, options: nil)[0] as? TVShowView
        view?.loadView(with: viewModel)
        
        XCTAssert(view?.showTitle.text == "Test", "Failed to set title")
        XCTAssert(view?.showSummary.text == "test summary", "Failed to set summary")
    }
    
    func testDaysSince() {
        let today = Date()
        let yesterday = today.addingTimeInterval(TimeInterval(floatLiteral: -86400))
        let daysElapsed = Date.daysSince(fromDate: yesterday, toDate: today, adjustBy: 0)
        XCTAssert(daysElapsed == 1, "Days Elapsed should be one!")
    }
    
    func testPrettyString() {
        let testVal = 1000
        XCTAssert(prettyString(from: testVal) == "Days since the show first premiered: 1,000", "Text should be: Days since the show first premiered: 1,000")
    }

}
